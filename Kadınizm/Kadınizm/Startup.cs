﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kadınizm.Startup))]
namespace Kadınizm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
